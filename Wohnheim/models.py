from django.db import models
from django.utils import timezone

class PositionData(models.Model):
    created_at  =   models.DateTimeField(default = timezone.now)
    field1      =   models.PositiveIntegerField()
    field2      =   models.PositiveIntegerField()
    field3      =   models.PositiveIntegerField()
    field4      =   models.PositiveIntegerField()
    field5      =   models.PositiveIntegerField()
    field6      =   models.PositiveIntegerField()
    field7      =   models.PositiveIntegerField()
    field8      =   models.PositiveIntegerField()

    def __str__(self):
        return '{:04d} | {}'.format(self.id, self.created_at.strftime("%H:%M %d.%m.%y %Z"))

    # def save(self, *arg, **kwargs):
    #     if not self.id:
    #         self.created_at = timezone.now()

    #     return super(PositionData, self).save(*arg, **kwargs)