# Generated by Django 2.2 on 2019-07-23 19:17

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PositionData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('field1', models.PositiveIntegerField()),
                ('field2', models.PositiveIntegerField()),
                ('field3', models.PositiveIntegerField()),
                ('field4', models.PositiveIntegerField()),
                ('field5', models.PositiveIntegerField()),
                ('field6', models.PositiveIntegerField()),
                ('field7', models.PositiveIntegerField()),
                ('field8', models.PositiveIntegerField()),
            ],
        ),
    ]
