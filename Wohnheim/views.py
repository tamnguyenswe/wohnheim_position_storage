from rest_framework     import  viewsets
from .models            import  PositionData
from .serializers       import  PositionDataSerializer
from rest_framework.response    import  Response

from rest_framework import mixins

# only allow to create, list and retrieve. Users cannot change already exist values
class PositionDataView( mixins.CreateModelMixin,
                        mixins.ListModelMixin,
                        mixins.RetrieveModelMixin,
                        viewsets.GenericViewSet):

    queryset            =   PositionData.objects.all()
    serializer_class    =   PositionDataSerializer

    def retrieve(self, request, pk = None):

        if (pk == 'last'):
            # get lastest object created
            last_object =   PositionData.objects.last()
            serializer  =   self.get_serializer(last_object)
            return Response(serializer.data)

        return super(PositionDataView, self).retrieve(self, request, pk)

# Create your views here.