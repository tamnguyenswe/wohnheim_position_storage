from rest_framework import serializers
from .models    import  PositionData

class PositionDataSerializer(serializers.Serializer):
    id          =   serializers.ReadOnlyField()
    created_at  =   serializers.DateTimeField(format='%H:%M %d.%m.%Y %Z', required = False)
    field1      =   serializers.IntegerField()
    field2      =   serializers.IntegerField()
    field3      =   serializers.IntegerField()
    field4      =   serializers.IntegerField()
    field5      =   serializers.IntegerField()
    field6      =   serializers.IntegerField()
    field7      =   serializers.IntegerField()
    field8      =   serializers.IntegerField()

    def create(self, validated_data):
        return PositionData.objects.create(**validated_data)
