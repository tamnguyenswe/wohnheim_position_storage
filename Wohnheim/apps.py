from django.apps import AppConfig


class WohnheimConfig(AppConfig):
    name = 'Wohnheim'
