from django.urls import path, include
from . import views
from rest_framework import routers

router  =   routers.DefaultRouter()
router.register('position', views.PositionDataView)

urlpatterns = [
    # url('last/', views.last, name = 'view-last'),
    path('', include(router.urls)), 
]